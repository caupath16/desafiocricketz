import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex: 1, 
        height: 100, 
        alignItems: "center", 
        justifyContent: "center"
    },
    image: {
        width: 40, 
        height: 40
    },
    text: {
        fontWeight: "bold", 
        fontSize: 16, 
        color: '#424242'
    }
})