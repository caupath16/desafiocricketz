import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        borderRadius: 20, 
        width: 12, 
        height: 12, 
        backgroundColor: '#1976d2', 
        alignSelf: "flex-end", 
        marginRight: 16, 
        marginTop: 16
    }
})