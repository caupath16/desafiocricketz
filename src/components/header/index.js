import React, { Component } from 'react'
import { Image, View, TouchableOpacity, StyleSheet, Text } from 'react-native'
import { styles } from './styles'

const person = require('DesafioCricketz/src/assets/person.jpg')
const chat_icon = require('DesafioCricketz/src/assets/chat_icon.png')

export default class CustomHeader extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity>
                    <Image source={person} style={styles.profilePicture} />
                </TouchableOpacity>
                <Text>RELOAD</Text>
                <TouchableOpacity>
                    <Image source={chat_icon} style={styles.chatPicture} />
                </TouchableOpacity>
            </View>
        )
    }
}