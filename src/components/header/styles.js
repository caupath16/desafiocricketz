import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginHorizontal: 16,
        alignItems: "center",
        backgroundColor: '#fff'
    },
    profilePicture: {
        width: 48,
        height: 48,
        borderRadius: 48 / 2,
        marginBottom: 5,
        marginTop: 5
    },
    chatPicture: {
        width: 32,
        height: 32
    }
})