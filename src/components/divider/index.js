import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'

export default class Divider extends Component {
    render(){
        return(
            <View style={this.props.hasMargin ? styles.lineWithMargin : styles.line}/>
        )
    }
}

const styles = StyleSheet.create({
    line: {
        height: 1,
        backgroundColor: '#e0e0e0',
    },
    lineWithMargin: {
        height: 1,
        backgroundColor: '#e0e0e0',
        marginHorizontal: 16
    }
})