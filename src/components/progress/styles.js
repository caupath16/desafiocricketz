import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    viewLabels: {
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center"
    },
    description: {
        fontSize: 10, 
        fontWeight: "bold", 
        color: '#bdbdbd'
    },
    label: {
        fontSize: 20, 
        fontWeight: "bold", 
        color: '#bdbdbd'
    }
})