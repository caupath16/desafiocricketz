import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import ProgressCircle from 'react-native-progress-circle'
import { styles } from './styles'


export default class Progress extends Component {
    render() {
        return (
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 16, marginTop: 20 }}>
                <ProgressCircle
                    percent={80}
                    radius={50}
                    borderWidth={5}
                    color="#3399FF"
                    shadowColor="#eeeeee"
                    bgColor="#fff">
                    <View style={styles.viewLabels}>
                        <Text style={styles.description}>{'Ran'}</Text>
                        <Text style={styles.label}>{'5.1'}</Text>
                        <Text style={styles.description}>{'miles'}</Text>
                    </View>
                </ProgressCircle>
                <ProgressCircle
                    percent={80}
                    radius={50}
                    borderWidth={5}
                    color="#3399FF"
                    shadowColor="#eeeeee"
                    bgColor="#fff">
                     <View style={styles.viewLabels}>
                        <Text style={styles.description}>{'Pace'}</Text>
                        <Text style={styles.label}>{'10.8'}</Text>
                        <Text style={styles.description}>{'Min/mi'}</Text>
                    </View>
                </ProgressCircle>
                <ProgressCircle
                    percent={80}
                    radius={50}
                    borderWidth={5}
                    color="#3399FF"
                    shadowColor="#eeeeee"
                    bgColor="#fff">
                     <View style={styles.viewLabels}>
                        <Text style={styles.description}>{'Max Hr'}</Text>
                        <Text style={styles.label}>{'144'}</Text>
                        <Text style={styles.description}>{'Bpm'}</Text>
                    </View>
                </ProgressCircle>
            </View>

        )
    }
}