import React, { Component } from 'react';
import { View, StyleSheet, Text, StatusBar, SafeAreaView, Image, Dimensions, ScrollView } from 'react-native'
import CustomHeader from '../../components/header';
import Divider from '../../components/divider';
import Top from '../../components/top';
import { styles } from './styles'

import Progress from '../../components/progress';

import Resume from '../../components/resume';

export default class LifeStyle extends Component {
    render() {
        return (
            <View style={styles.container}>
                <SafeAreaView style={styles.safeArea}>
                    <StatusBar translucent backgroundColor="#fff" barStyle="dark-content" />
                    <CustomHeader />
                    <Divider />
                    <ScrollView>
                        <View style={styles.viewScroll}>
                            <Top />
                            <Divider />
                            <Progress />
                            <Resume/>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        )
    }
}